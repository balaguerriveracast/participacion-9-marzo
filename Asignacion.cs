//crear un programa que simule un banco que tiene 3 clientes que pueden hacer depositos y retiros. Tambien el banco requiere que al final del dia calcule la cantidad de dinero que hay depositado
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tarea
{
  class Cliente 
  {
    private string nombre;
    private int monto;

    public Cliente(string n)
    {
      nombre= n;
      monto= 0;

    }
    public void depositar(int d)
    {
      monto= monto+d;
    }
    public void retirar(int r)
    {
      monto= monto-r;
    } 
    public int retornar()
    {
      return monto;
    }
    public void mostrar()
    {
      Console.WriteLine("{0} deposito {1}" ,nombre, monto);

    }
  }
  class Banco
  {
    private Cliente cliente1, cliente2, cliente3;

    public Banco()
    {
      cliente1= new Cliente("Alejandro");
      cliente2= new Cliente("Maria");
      cliente3= new Cliente("Leo");
    }
    public void depositar2()
    {
      cliente1.depositar(200);
      cliente2.depositar(100);
      cliente3.depositar(300);

    }
    public void total()
    {
      int tl = cliente1.retornar() + cliente2.retornar() + cliente3.retornar();
      Console.WriteLine("El total de dinero depositado= {0}"  ,tl);
      cliente1.mostrar();
      cliente2.mostrar();
      cliente3.mostrar();
    
    }
    static void Main(string[] args)
    {
      Banco banco2= new Banco();
      banco2.depositar2();
      banco2.total();
      Console.ReadKey();
    }
  }
}